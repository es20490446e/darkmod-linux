#! /bin/bash
set -e
enable sleep

builders="$(realpath "$(dirname "${0}")")"
project="$(cd "${builders}"; cd ..; pwd)"
build="${project}/_BUILD/1-workspace"

getStableSource="${build}.../1-getStableSource"
getDevelSource="${build}.../2-getDevelSource"
buildDevelLibs="${build}.../3-buildDevelLibs"
buildStableWorkspace="${build}.../4-buildStableWorkspace"
buildDevelWorkspace="${build}.../5-buildDevelWorkspace"
buildInstaller="${build}.../6-buildInstaller"


mainFunction () {
	if [[ ! -d "${build}" ]]; then
		getStableSource
		getDevelSource
		buildDevelLibs
		buildStableWorkspace
		buildDevelWorkspace
		buildInstaller
		so mv "${build}..." "${build}"
	fi
}


buildDevelLibs () {
	if [[ ! -d "${buildDevelLibs}" ]]; then
		so cp --recursive "${getDevelSource}/ThirdParty" "${buildDevelLibs}..."
		cd "${buildDevelLibs}..."

		so rm --recursive --force "artefacts"

		installConan
		compileLibs
		uninstallConan

		cd "${build}..."
		so mv "${buildDevelLibs}..." "${buildDevelLibs}"
	fi
}


buildDevelWorkspace () {
	if [[ ! -d "${buildDevelWorkspace}" ]]; then
		so mkdir --parents "${buildDevelWorkspace}.../darkmod"
		cd "${buildDevelWorkspace}..."

		so cp --recursive "${getDevelSource}" "darkmod_src"
		patchDir "${patches}/buildDevelWorkspace" "${buildDevelWorkspace}..."

		so rm --recursive --force "darkmod_src/ThirdParty"
		deblob
		so cp --recursive --force \
			"${buildDevelLibs}" \
			"darkmod_src/ThirdParty"

		cd "${build}..."
		so mv "${buildDevelWorkspace}..." "${buildDevelWorkspace}"
	fi
}


buildInstaller () {
	if [[ ! -d "${buildInstaller}" ]]; then
		so cp --recursive "${buildDevelWorkspace}/darkmod_src" "${buildInstaller}..."
		cd "${buildInstaller}.../tdm_installer"

		compile

		cd "${build}..."
		so mv "${buildInstaller}..." "${buildInstaller}"
	fi
}


buildStableWorkspace () {
	if [[ ! -d "${buildStableWorkspace}" ]]; then
		so mkdir --parents "${buildStableWorkspace}.../darkmod"
		cd "${buildStableWorkspace}..."

		so cp --recursive "${getStableSource}" "darkmod_src"
		removeForeingArtefacts "./darkmod_src/ThirdParty/artefacts"
		so cp --recursive --force \
			"${buildDevelLibs}/artefacts" \
			"darkmod_src/ThirdParty/"

		cd "${build}..."
		so mv "${buildStableWorkspace}..." "${buildStableWorkspace}"
	fi
}


compileLibs () {
	yes "yes"| so python "1_export_custom.py"

	so conan install . \
		--profile:build profiles/base_linux \
		--profile profiles/os_linux \
		--profile profiles/arch_64 \
		--profile profiles/build_release \
		--settings thedarkmod/*:build_type=Release \
		--output-folder artefacts/linux_64 \
		--deployer tdm_deploy \
		--build missing
}


configureConan () {
	if [[ ! "${PATH}" =~ "${HOME}/.local/bin" ]]; then
		export PATH="${HOME}/.local/bin:${PATH}"
	fi

	export CONAN_HOME="${project}/_CONAN"
}


getStableSource () {
	if [[ ! -d "${getStableSource}" ]]; then
		so mkdir --parents "${getStableSource}..."
		cd "${getStableSource}..."

		local tag; tag="$("${project}/version.sh" "tag")"
		so svn checkout "https://svn.thedarkmod.com/publicsvn/darkmod_src/tags/${tag}/" .

		cd "${build}..."
		so mv "${getStableSource}..." "${getStableSource}"
	fi
}


getDevelSource () {
	if [[ ! -d "${getDevelSource}" ]]; then
		so mkdir --parents "${getDevelSource}..."
		cd "${getDevelSource}..."

		so svn checkout "https://svn.thedarkmod.com/publicsvn/darkmod_src/trunk/" .

		cd "${build}..."
		so mv "${getDevelSource}..." "${getDevelSource}"
	fi
}


installConan () {
	trap "uninstallPythonModule conan; exit 1" INT TERM QUIT HUP ERR EXIT
	installPythonModule "conan"
	configureConan
}


installPythonModule () {
	local module="${1}"
	yes | so pipx install "${module}"
}


prepareEnvironment () {
	so renice --priority 10 "$$"
	source "${builders}/0-shared.sh"
	removeIncompleteDirs "${build}..."
}


removeBlobs () {
	local artefactsDirs; artefactsDirs=("${@}")

	so rm --recursive --force "${artefactsDirs[@]}"
	blobs | xargs -rd '\n' rm --force
}


removeForeingArtefacts () {
	local dir; dir="${1}"

	find "${dir}" \
		-type d \
		-name 'lnx32_s_gcc_rel_stdcpp' \
		-or -name 'win??_s_vc_rel_*' |
	xargs rm --recursive --force
}


uninstallConan () {
	yes | so rm --recursive "${CONAN_HOME}"
	uninstallPythonModule "conan"
	trap - INT TERM QUIT HUP ERR EXIT
}


uninstallPythonModule () {
	local module="${1}"
	yes | so pipx uninstall "${module}"
}


yes () {
	while true; do
		/usr/bin/yes "${@}"
		builtin sleep 0.1
	done
}


prepareEnvironment
mainFunction
