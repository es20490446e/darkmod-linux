#! /bin/bash
set -e

platform="lnx64_s_gcc_rel_stdcpp"
builders="$(realpath "$(dirname "${0}")")"
project="$(cd "${builders}"; cd ..; pwd)"
patches="${project}/patches"

#shellcheck disable=SC2034
export MAKEFLAGS="-j$(nproc)"


checkPreviousSteps () {
	local step steps; steps=("${@}")
	local missing=()

	for step in "${steps[@]}"; do
		if [[ ! -d "${project}/_BUILD/${step}" ]]; then
			missing+=("${step}")
		fi
	done

	if [[ "${#missing[@]}" -ne 0 ]]; then
		echo "Missing previous steps: ${missing[*]}"
		exit 1
	fi
}


compile () {
	local flags="${1}"

	so cmake . \
		-D CMAKE_BUILD_TYPE="Release" \
		-D THIRDPARTY_PLATFORM_OVERRIDE="${platform}" \
		-D CMAKE_EXE_LINKER_FLAGS="${flags}"
	so make
}


deblob () {
	blobs | xargs -rd '\n' rm --force
}


patchDir () {
	local inputDir="${1}"
	local outputDir="${2}"

	so cp \
		--recursive \
		--no-target-directory \
		--force \
		"${inputDir}" "${outputDir}"
}


removeIncompleteDirs () {
	local dir="${1}"

	if [[ -d "${dir}" ]]; then
		find "${dir}" \
			-mindepth 1 \
			-maxdepth 1 \
			-type d \
			-name '*...' \
			-print0 |
		xargs -0 rm --recursive --force
	fi
}


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}
