#! /bin/bash
set -e

builders="$(realpath "$(dirname "${0}")")"
project="$(cd "${builders}"; cd ..; pwd)"
build="${project}/_BUILD"


mainFunction () {
	local workspace="${build}/1-workspace/4-buildStableWorkspace"
	local engine="${build}/3-engine"

	if [[ ! -d "${engine}" ]]; then
		so cp --recursive "${workspace}" "${engine}..."
		cd "${engine}.../darkmod_src"

		compile "-no-pie"

		cd "${build}"
		so mv "${engine}..." "${engine}"
	fi
}


prepareEnvironment () {
	so renice --priority 10 "$$"
	source "${builders}/0-shared.sh"
	checkPreviousSteps "1-workspace"
	removeIncompleteDirs "${build}"
}


prepareEnvironment
mainFunction
