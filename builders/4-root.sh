#! /bin/bash
set -e

builders="$(realpath "$(dirname "${0}")")"
project="$(cd "${builders}"; cd ..; pwd)"
assets="${project}/assets"

build="${project}/_BUILD"
contents="${build}/2-contents"
engine="${build}/3-engine/darkmod"

root="${project}/_ROOT"
darkmod="${root}.../usr/share/darkmod"
linked="${darkmod}/linked"


mainFunction () {
	if [[ ! -d "${root}" ]]; then
		buildDarkmodDir
		buildApplicationsDir
		buildBinDir

		so chmod --recursive u=rwX,g=rX,o=rX "${root}..."
		mv "${root}..." "${root}"
	fi
}


buildApplicationsDir () {
	local applications="${root}.../usr/share/applications"
	so mkdir --parents "${applications}"
	so cp "${assets}/darkmod.desktop" "${applications}/"
	so cp "${assets}/darkmod-missions.desktop" "${applications}/"
}


buildBinDir () {
	local bin="${root}.../usr/bin"
	so mkdir --parents "${bin}"

	so cp "${assets}/darkmod" "${bin}/"
	so cp "${assets}/darkmod-missions" "${bin}/"

	so chmod u=rwX,g=rX,o=rX "${bin}/darkmod"
	so chmod u=rwX,g=rX,o=rX "${bin}/darkmod-missions"
}


buildDarkmodDir () {
	buildLinkedDir
	buildReplicatedDir
	buildOverwrittenDir
}


buildLinkedDir () {
	so mkdir --parents "${darkmod}"
	so cp --recursive "${contents}" "${linked}"

	cd "${linked}"
	so rm --recursive tdm_installer* .zipsync
	find . -empty -type d -delete
	deblob
}


buildOverwrittenDir () {
	local overwritten="${darkmod}/overwritten"
	so mkdir --parents "${overwritten}"
	so cp "${engine}/thedarkmod.custom" "${overwritten}/thedarkmod.x64"

	local version; version="$("${project}/version.sh")"
	echo "${version}" > "${overwritten}/version.txt"
}


buildReplicatedDir () {
	local replicated="${darkmod}/replicated"
	so mkdir --parents "${replicated}"
	so mv "${linked}"/*.ini "${replicated}/"
	so mv "${linked}/fms" "${replicated}/"
}


prepareEnvironment () {
	so renice --priority 10 "$$"
	source "${builders}/0-shared.sh"
	checkPreviousSteps "2-contents" "3-engine"
	removeIncompleteDirs "${project}..."
}


prepareEnvironment
mainFunction
