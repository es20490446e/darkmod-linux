#! /bin/bash
set -e

builders="$(realpath "$(dirname "${0}")")"
project="$(cd "${builders}"; cd ..; pwd)"
build="${project}/_BUILD"


mainFunction () {
	local contents="${build}/2-contents"
	local installer="${build}/1-workspace/6-buildInstaller/tdm_installer"

	if [[ ! -d "${contents}" ]]; then
		so mkdir --parents "${contents}..."
		cd "${contents}..."

		so cp --force "${installer}/tdm_installer.linux64" "tdm_installer.x64"
		timeout --foreground --kill-after=5 3600 so xvfb-run ./tdm_installer.x64 --unattended

		cd "${build}"
		so mv "${contents}..." "${contents}"
	fi
}


prepareEnvironment () {
	so renice --priority 10 "$$"
	source "${builders}/0-shared.sh"
	checkPreviousSteps "1-workspace"
	removeIncompleteDirs "${build}"
}


prepareEnvironment
mainFunction
