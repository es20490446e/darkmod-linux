#! /bin/bash
set -e


mainFunction () {
	tags="$(tags)"

	if [[ "${#}" -eq 0 ]]; then		
		local gameVersion; gameVersion="$(gameVersion)"
		local packagerVersion; packagerVersion="$(packagerVersion)"
		echo "${gameVersion}.${packagerVersion}"
	elif [[ "${#}" -gt 1 ]]; then
		echo "Surplus arguments" >&2
		exit 1
	elif [[ "${1}" == "game" ]]; then
		gameVersion
	elif [[ "${1}" == "packager" ]]; then
		packagerVersion
	elif [[ "${1}" == "tag" ]]; then
		tag
	else
		echo "Invalid argument: ${1}" >&2
		exit 1
	fi
}


echoIfNonEmpty () {
	local element="${1}"
	local contents="${2}"

	if [[ -z "${contents}" ]]; then
		retrieveFailed "${element}"
	fi
	
	echo "${contents}"
}


gameVersion () {
	local tag; tag="$(tag)"
	local mainVersion; mainVersion="$(mainVersion "${tag}")"
	local revision; revision="$(revision "${mainVersion}")"
	
	echo "${mainVersion}.${revision}"
}


mainVersion () {
	local tag="${1}"
	
	echo "${tag}" |
	tr --delete "[:alpha:]"
}


packagerVersion () {
	local version; version="$(
		curl --silent "https://gitlab.com/es20490446e/darkmod-linux" |
		grep "project-stat-value" |
		grep "commits" |
		cut --delimiter='>' --fields=7 |
		cut --delimiter='<' --fields=1
	)"
	
	echoIfNonEmpty "packager version" "${version}"
}


retrieveFailed () {
	local element="${1}"
	
	echo "Unable to retrieve ${element}" >&2
	echo "Ask a developer to fix this" >&2
	exit 1
}


revision () {
	local mainVersion="${1}"
	
	local revision; revision="$(
		echo "${tags}" |
		grep --count "${mainVersion}"
	)"
	
	if [[ "${revision}" -eq 0 ]]; then
		retrieveFailed "revision"
	else
		echo "$(("${revision}" - 1))"
	fi
}


tag () {
	echo "${tags}" |
	tail -n1
}


tags () {
	local svn="https://svn.thedarkmod.com/publicsvn/darkmod_src/tags/"

	local tags; tags="$(
		curl --silent --connect-timeout 1 --retry 3 "${svn}" |
		grep 'href=' |
		cut --delimiter='>' --fields=3 |
		cut --delimiter='/' --fields=1 |
		grep '^[0-9]'
	)"
	
	echoIfNonEmpty "tags" "${tags}"
}


mainFunction "${@}"
