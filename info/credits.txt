
For a list of contributors to this repository just see:
https://gitlab.com/es20490446e/darkmod-linux/-/graphs/master

Even when this code has greatly been improved since then, many of the algorithms of it were thanks to the packagers of the game for Arch Linux. Aka:

- Michael Bryant (Shadow53)
- Benjamin Colard <benjamin at colard dot be>

