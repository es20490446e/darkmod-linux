#! /bin/bash
set -e

here="$(realpath "$(dirname "${0}")")"
builders="${here}/builders"


mainFunction () {
	"${builders}"/1-workspace.sh

	"${builders}"/2-contents.sh &
	local contents="$!"

	"${builders}"/3-engine.sh &
	local engine="$!"

	waitSubprocesses
	"${builders}"/4-root.sh
}


checkDependencies () {
	local missing=()

	local dependencyLists=(
		"${here}/info/dependencies.txt"
		"${here}/info/dependencies/building.txt"
		"${here}/info/dependencies/installing.txt"
	)

	for dependencyList in "${dependencyLists[@]}"; do
		if [[ -f "${dependencyList}" ]]; then
			local lines; readarray -t lines < <(awk 'NF' "${dependencyList}")

			for line in "${lines[@]}"; do
				local name; name="$(echo "${line}" | cut --delimiter='"' --fields=2)"
				local path; path="$(echo "${line}" | cut --delimiter='"' --fields=4)"
				local web; web="$(echo "${line}" | cut --delimiter='"' --fields=6)"

				if [[ -n "${web}" ]]; then
					local web="(${web})"
				fi

				if [[ ! -f "${path}" ]]; then
					missing+=("${name}  ${web}")
				fi
			done
		fi
	done

	if [[ "${#missing[@]}" -gt 0 ]]; then
		echo "Missing required software:" >&2
		echo >&2
		printf '%s\n' "${missing[@]}" >&2
		echo >&2
		echo "Get those installed first"
		echo "and run this program again"
		exit 1
	fi
}


prepareEnvironment () {
	so renice --priority 10 "$$"
	cd "${here}"
	checkDependencies
}


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


terminateSubprocesses () {
	pkill -P "${contents}" &>/dev/null || true; pkill -P "${engine}" &>/dev/null || true
}


waitSubprocesses () {
	trap 'terminateSubprocesses' ERR INT TERM QUIT EXIT
	wait
	trap - ERR INT TERM QUIT EXIT
}


prepareEnvironment
mainFunction
